MACHINES API Example Frontend:

REQUIREMENTS:
1.This app needs Exercise 1 to work: 

Clone on a separate directory and follow the installation instructions:
git clone https://osmunlo66@bitbucket.org/osmunlo66/machineexercises.git

2. React installed in the computer running the project
3. npm installed in the computer running the project

This front end one-pager calls one of the API methods and displays all products within exercise 1's database in a non-JSON way. Very basic

INSTALL INSTRUCTIONS:

The API method called is machinesapi/listProducts.

1. Download the project and save into a folder
2. Edit index.js within the src folder 
3. Set the global variable DB_LIST_QUERY to your symfony localhost link and the endpoint above (exercise 1).
4. Save and close.
5. Access the project folder via CMD and run npm start.